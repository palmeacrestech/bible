//
//  FirstRunViewController.swift
//  Bible
//
//  Created by Douglas Palme on 4/16/21.
//

import UIKit
import DateTimePicker
import UserNotifications
import Purchases
import BackgroundTasks

class FirstRunViewController: UIViewController, DateTimePickerDelegate, UNUserNotificationCenterDelegate {

   let userNotificationCenter = UNUserNotificationCenter.current()
   private let taskIdentifier = "com.palmeacres.bible1.refresh"
   private let taskIdentifierProcessing = "com.palmeacres.bible1.process"
   
   
   @IBOutlet weak var lblTop: UILabel!
   @IBOutlet weak var lblBottom: UILabel!
   @IBOutlet weak var btnContinue: MyButton!
   @IBOutlet var viewPrimary: UIView!
   
   
   /// - Store the offering being displayed
   var offering: Purchases.Offering?
   
   override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.navigationItem.hidesBackButton = true
   }
   
    override func viewDidLoad() {
        super.viewDidLoad()
   
      BGTaskScheduler.shared.register(forTaskWithIdentifier: taskIdentifier, using: DispatchQueue.global()) { task in
         self.handleAppRefresh(task)
      }
      
      BGTaskScheduler.shared.register(forTaskWithIdentifier: taskIdentifierProcessing, using: DispatchQueue.global()) { task in
         self.databaseCleanup(task)
      }
      
      if UserDefaults.standard.object(forKey: "Reviewed") == nil {
         UserDefaults.standard.setValue(0, forKey: "Reviewed")
      }
      
      // check to see if review UserDefault exists, if not add it.
      if reviewExists() == false {
         // Create it
         UserDefaults.standard.setValue(0, forKey: "ReviewCt")
      }
      
      if chkRenewal() == true {
         // set status
      } else {
         // turn it off
      }
      
      // turn off view
      viewPrimary.isHidden = true
      // Assigning self delegate on userNotificationCenter
      self.userNotificationCenter.delegate = self
      // first check to see if user has already registered
      if UserDefaults.standard.integer(forKey: "firstRun") == 1 {
         if UserDefaults.standard.bool(forKey: "isPlaying") == true {
            let musicTrack = UserDefaults.standard.value(forKey: "musicTrack")
            Music.StartPlaying(musicName: musicTrack as! String)
         }
         guard let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return }
         navigationController?.pushViewController(myVC, animated: true)
      } else {
         UserDefaults.standard.setValue(0, forKey: "isSubscribed")
         // set default music track
         UserDefaults.standard.setValue("Beyond-The-Summit", forKey: "musicTrack")
         viewPrimary.isHidden = false
         UserDefaults.standard.setValue(true, forKey: "isPlaying")
         let musicTrack = UserDefaults.standard.value(forKey: "musicTrack")
         Music.StartPlaying(musicName: musicTrack as! String)
      }
    }
    
   private func handleAppRefresh(_ task: BGTask) {
      let myNotifications = Notifications()
      let queue = OperationQueue()
      queue.maxConcurrentOperationCount = 1
      let appRefreshOperation = BlockOperation {
         myNotifications.getVerse()
      }
      queue.addOperation(appRefreshOperation)
      task.expirationHandler = {
         queue.cancelAllOperations()
      }
      let lastOperation = queue.operations.last
      lastOperation?.completionBlock = {
         task.setTaskCompleted(success: !(lastOperation?.isCancelled ?? false ))
      }
      scheduleAppRefresh()
   }
   
   private func databaseCleanup(_ task: BGTask) {
      // DB Cleanup
   }
   
   private func scheduleDatabaseClean() {
      do {
         let request = BGProcessingTaskRequest(identifier: taskIdentifierProcessing)
         request.requiresExternalPower = false
         request.requiresNetworkConnectivity = true
         try BGTaskScheduler.shared.submit(request)
      } catch {
         print(error.localizedDescription)
      }
   }
   
   private func scheduleAppRefresh() {
      do {
         let request = BGAppRefreshTaskRequest(identifier: taskIdentifier)
         request.earliestBeginDate = Date(timeIntervalSinceNow: 1 * 60)
         try BGTaskScheduler.shared.submit(request)
      } catch {
         print(error.localizedDescription)
      }
   }
   
   func reviewExists() -> Bool {
      var retVal = false
      if UserDefaults.standard.object(forKey: "ReviewCt") != nil {
        retVal = true
      }
      return retVal
   }
   
   func chkRenewal() -> Bool {
       var RetVal: Bool = false
       Purchases.shared.purchaserInfo { (purchaserInfo, error) in
         // get user id
         _ = Purchases.shared.appUserID
         // access latest purchaserInfo
         if purchaserInfo?.activeSubscriptions.isEmpty == false {
           //if !(purchaserInfo?.entitlements.active.isEmpty == false ) {
               //user has access to some entitlement
               if purchaserInfo?.entitlements["Monthly"]?.isActive == true {
                   RetVal = true
                   UserDefaults.standard.set("Monthly", forKey: "PurType")
                  UserDefaults.standard.setValue(1, forKey: "isSubscribed")
               } else {
                  if purchaserInfo?.entitlements["Yearly"]?.isActive == true {
                     RetVal = true
                     UserDefaults.standard.set("Yearly", forKey: "PurType")
                     UserDefaults.standard.setValue(1, forKey: "isSubscribed")
                  }
               }
           } else {
               UserDefaults.standard.set("", forKey: "PurType")
               RetVal = false
           }
       }
       return RetVal
   }
   
   
   @IBAction func btnContinue(_ sender: Any) {
      switch btnContinue.titleLabel?.text {
      case "continue":
         lblTop.font = .preferredFont(forTextStyle: .title3)
         lblTop.text = "We would like to thank you for downloading our app, our hope and prayer is that you find the app not only useful but each verse an inspiration to you as you progress through your day."
         lblBottom.text = "Enjoy!"
         btnContinue.setTitle("Get Started", for: .normal)
      case "Get Started":
         lblTop.text = ""
         lblBottom.text = ""
         btnContinue.setTitle("Schedule a Notification", for: .normal)
         timeSelector()
         print("Bubba")
      case "Scheuled Notification":
         lblTop.text = ""
         lblBottom.text = ""
         btnContinue.setTitle("Schedule a Notification", for: .normal)
         timeSelector()
      default:
         timeSelector()
      }
   }
   
   func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
      title = picker.selectedDateString
      title = picker.selectedDateString
   }
   
   func timeSelector() {
      //presentTimeSelector()
      let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
      let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
      let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
   
      picker.locale = Locale(identifier: "en_US")

      picker.is12HourFormat = true
      picker.isTimePickerOnly = true
      picker.includesSecond = false
      picker.dateFormat = "hh:mm aa"
      picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
      picker.doneButtonTitle = "Schedule Notication"
      picker.doneBackgroundColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
      picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 20))
      picker.dismissHandler = {
         picker.removeFromSuperview()
      }
      picker.completionHandler = { date in
         let formatter = DateFormatter()
         formatter.dateFormat = "HH:mm"
         formatter.timeZone = NSTimeZone.local
         let myDate = formatter.string(from: date)

         let newDate = formatter.date(from: myDate)
         self.requestNotificationAuthorization(myDate: newDate!)

         // move to next view controller
         UserDefaults.standard.setValue(1, forKey: "firstRun")
         guard let myVC1 = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as?
                  SubscriptionViewController else { return }
         self.navigationController?.pushViewController(myVC1, animated: true)

         //guard let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return }
         //self.navigationController?.pushViewController(myVC, animated: true)
      }
      picker.delegate = self
      // or show it like a modal
      picker.show()
   }
   
   func requestNotificationAuthorization(myDate: Date) {
       let authOptions = UNAuthorizationOptions.init(arrayLiteral: .alert, .badge, .sound)
       self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
         if let error = error {
            print(error.localizedDescription)
         } else {
            self.sendNotification(myDate: myDate)
         }
       }
   }
   
   
   func sendNotification(myDate: Date) {
      // Swift
      var amPm: String = "AM"
      var myTimeHour: Int = 00
      let content = UNMutableNotificationContent()
      content.title = "@Bible"
      content.body = "Don't miss out on today's inspiring verse, selected just for you"
      content.badge = 1
      content.sound = .default
      
      let triggerDaily = Calendar.current.dateComponents([.hour, .minute], from: myDate)
      let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
      let identifier = "DailyTime1"
      let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

      userNotificationCenter.add(request, withCompletionHandler: { (error) in
         if error != nil {
               print("Error")
           }
       })
   
      // Save off the date
      if triggerDaily.hour! < 12 {
         amPm = "AM"
         myTimeHour = triggerDaily.hour!
      } else {
         myTimeHour = triggerDaily.hour! - 12
         amPm = "PM"
      }
      let svTime = String(format: "%02d", myTimeHour) + ":" + String(format: "%02d", triggerDaily.minute!) + " " + amPm
      UserDefaults.standard.setValue(svTime, forKey: "DailyTime1")
   }
   
   func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
       completionHandler()
   }

   func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      completionHandler([.banner, .badge, .sound])
   }
   
}
