//
//  NotificationsViewController.swift
//  Bible
//
//  Created by Douglas Palme on 5/19/21.
//

import UIKit
import DateTimePicker
import UserNotifications
import BackgroundTasks

class NotificationsViewController: UIViewController, DateTimePickerDelegate, UNUserNotificationCenterDelegate {
   
   let userNotificationCenter = UNUserNotificationCenter.current()

   
   
   @IBOutlet weak var lblNotification1: UILabel!
   @IBOutlet weak var lblNotification2: UILabel!
   @IBOutlet weak var lblNotification3: UILabel!
   @IBOutlet weak var lblNotification4: UILabel!
   @IBOutlet weak var lblNotification5: UILabel!
   
   @IBOutlet weak var lblValue1: UIButton!
   @IBOutlet weak var lblValue2: UIButton!
   @IBOutlet weak var lblValue3: UIButton!
   @IBOutlet weak var lblValue4: UIButton!
   @IBOutlet weak var lblValue5: UIButton!
   
   
   var isSubscribed: Int = 0
   var NotificationCt: Int = 0
   var myNotifications = Notifications()
   
   override func viewWillAppear(_ animated: Bool) {
      // Check to see if user is subscribed.
      if UserDefaults.standard.value(forKey: "PurType") != nil {
         if UserDefaults.standard.value(forKey: "PurType") as! String != "" {
            // we have a subscription that is still valied
            isSubscribed = 1
         } else {
            isSubscribed = 0
         }
      } else {
         isSubscribed = 0
      }
   }
   

   override func viewDidLoad() {
      super.viewDidLoad()
      
      

      
      
      loadScreen()
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      self.navigationController!.navigationBar.tintColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
      _ = UIBarButtonItemAppearance()
      userNotificationCenter.delegate = self
   }
   
   func loadScreen() {
      NotificationCt = 0
      DispatchQueue.main.async {
      // always display notification 1 if there is a value
      if UserDefaults.standard.value(forKey: "DailyTime1") != nil {
         let title = UserDefaults.standard.value(forKey: "DailyTime1")
         self.lblValue1.setTitle((title as! String), for: .normal)
         self.lblNotification1.isHidden = false
         self.lblValue1.isHidden = false
         self.NotificationCt += 1
      } else {
         self.lblValue1.isHidden = true
         self.lblNotification1.isHidden = true
      }
         self.isSubscribed = 1 // testing
         if self.isSubscribed == 0 {
         // We turn off
            self.lblNotification2.isHidden = true
            self.lblValue2.isHidden = true
            self.lblNotification3.isHidden = true
            self.lblValue3.isHidden = true
            self.lblNotification4.isHidden = true
            self.lblValue4.isHidden = true
            self.lblNotification5.isHidden = true
            self.lblValue5.isHidden = true
         } else {
            // set values
            if UserDefaults.standard.value(forKey: "DailyTime2") != nil {
               self.NotificationCt += 1
               let title = UserDefaults.standard.value(forKey: "DailyTime2")
               self.lblValue2.setTitle(title as? String, for: .normal)
               self.lblNotification2.isHidden = false
               self.lblValue2.isHidden = false
            } else {
               self.lblNotification2.isHidden = true
               self.lblValue2.isHidden = true
            }
            if UserDefaults.standard.value(forKey: "DailyTime3") != nil {
               self.NotificationCt += 1
               let title = UserDefaults.standard.value(forKey: "DailyTime3")
               self.lblValue3.setTitle(title as? String, for: .normal)
               self.lblNotification3.isHidden = false
               self.lblValue3.isHidden = false
            } else {
               self.lblNotification3.isHidden = true
               self.lblValue3.isHidden = true
            }
         if UserDefaults.standard.value(forKey: "DailyTime4") != nil {
            self.NotificationCt += 1
            let title = UserDefaults.standard.value(forKey: "DailyTime4")
            self.lblValue4.setTitle(title as? String, for: .normal)
            self.lblNotification4.isHidden = false
            self.lblValue4.isHidden = false
         } else {
            self.lblNotification4.isHidden = true
            self.lblValue4.isHidden = true
         }
         if UserDefaults.standard.value(forKey: "DailyTime5") != nil {
            self.NotificationCt += 1
            let title = UserDefaults.standard.value(forKey: "DailyTime5")
            self.lblValue5.setTitle(title as? String, for: .normal)
            self.lblNotification5.isHidden = false
            self.lblValue5.isHidden = false
         } else {
            self.lblNotification5.isHidden = true
            self.lblValue5.isHidden = true
         }
      }
      }
   }

   func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
      title = picker.selectedDateString
      title = picker.selectedDateString
   }
   
   
   
   @IBAction func btnAdd(_ sender: Any) {
      // Check to see if we are full first, if so we display a message and leave
      // Display a message
      if self.NotificationCt == 5 {
         let refreshAlert = UIAlertController(title: "Maximum Notifications Reached", message: "You have reached the Maximum Notifications currently allowed.", preferredStyle: UIAlertController.Style.alert)
         refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            return
         }))
         present(refreshAlert, animated: true, completion: nil)
      } else {
      // Check to see if user is subscribed and if DailyTime1 has a value, if both are true, show message and send to subscription screen
         if isSubscribed == 0 {
            // Check to see if DailyTime1 has a value.
            if UserDefaults.standard.value(forKey: "DailyTime1") != nil {
               // Display message
               let aTitle = "Unlock Notifications"
               let aMessage = "Multiple Daily Notifications is a feature reserved for Patrons"
               let alert = UIAlertController(title: aTitle, message: aMessage, preferredStyle: .alert)
               let action = UIAlertAction(title: "OK", style: .default, handler: { _ in
               //self.navigationController?.popViewController(animated: true)
               let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
               self.navigationController?.pushViewController(myVC, animated: true)
            })
            alert.addAction(action)
            //alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.present(alert, animated: true)
         } else {
            // display setup for Notifications
            let myNotification = getNotificationNo()
            timeSelector(myNotification: myNotification)
         }
      } else {
         // allow user to create as many notifications as possible.
         let myNotification = getNotificationNo()
         print(myNotification)
         timeSelector(myNotification: myNotification)
      }
      }
   }

   func getNotificationNo() -> String {
      var RetVal: String = ""
      for x in 1...5 {
         let key = "DailyTime" + String(x)
         if UserDefaults.standard.value(forKey: key) == nil {
            RetVal = key
            break
         }
      }
      return RetVal
   }
   
   func timeSelector(myNotification: String) {
      //presentTimeSelector()
      let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
      let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
      let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
   
      picker.locale = Locale(identifier: "en_US")

      picker.is12HourFormat = true
      picker.isTimePickerOnly = true
      picker.includesSecond = false
      picker.dateFormat = "hh:mm aa"
      picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
      picker.doneButtonTitle = "Schedule Notication"
      picker.doneBackgroundColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
      picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 20))
      picker.dismissHandler = {
         picker.removeFromSuperview()
      }
      picker.completionHandler = { date in
         let formatter = DateFormatter()
         formatter.dateFormat = "HH:mm"
         formatter.timeZone = NSTimeZone.local
         let myDate = formatter.string(from: date)

         let newDate = formatter.date(from: myDate)
         self.requestNotificationAuthorization(myDate: newDate!, myNotification: myNotification)
      }
      picker.delegate = self
      // or show it like a modal
      picker.show()
   }
   
   
   
   func requestNotificationAuthorization(myDate: Date, myNotification: String) {
       let authOptions = UNAuthorizationOptions.init(arrayLiteral: .alert, .badge, .sound)
       self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
         if let error = error {
            print(error.localizedDescription)
         } else {
            self.sendNotification(myDate: myDate, myIdentifier: myNotification)
         }
       }
   }
   
   
   func sendNotification(myDate: Date, myIdentifier: String) {
      // Swift
      var amPm: String = "AM"
      var myTimeHour: Int = 00
      let content = UNMutableNotificationContent()
      content.title = "@Bible"
      content.body = "Don't miss out on today's inspiring verse, selected just for you"
      content.badge = 1
      content.sound = .default
      
      let triggerDaily = Calendar.current.dateComponents([.hour, .minute], from: myDate)
      let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
      let identifier = myIdentifier
      let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

      userNotificationCenter.add(request, withCompletionHandler: { (error) in
         if error != nil {
               print("Error")
            }
       })
      // Save off the date
      if triggerDaily.hour! < 12 {
         amPm = "AM"
         myTimeHour = triggerDaily.hour!
      } else {
         myTimeHour = triggerDaily.hour! - 12
         amPm = "PM"
      }
      let svTime = String(format: "%02d", myTimeHour) + ":" + String(format: "%02d", triggerDaily.minute!) + " " + amPm
      UserDefaults.standard.setValue(svTime, forKey: myIdentifier)
      self.loadScreen()
   }
   
   func userNotificationCenter(_ center: UNUserNotificationCenter,
                               didReceive response: UNNotificationResponse,
                               withCompletionHandler completionHandler: @escaping () -> Void) {
       if response.actionIdentifier == UNNotificationDismissActionIdentifier {
           print("We got a response")
       }
       else if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
           // The user launched the app
            print("User launched App")
       }
       print("Fucked up")
       // Else handle any custom actions. . .
   }
   
   
   // func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
   //    completionHandler()
   // }

   func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      completionHandler([.banner, .badge, .sound])
   }
   
   @IBAction func lblValue1Click() {
      let myName = "DailyTime1"
      // Prompt to Delete or change
      let refreshAlert = UIAlertController(title: "Modify Notification", message: "Would you like to Modify or Delete the Notification?", preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action:UIAlertAction) in
         print("do nothing")
      }))
      
      refreshAlert.addAction(UIAlertAction(title: "Modify Notification", style: .default, handler: { (action: UIAlertAction!) in
         self.timeSelector(myNotification: myName)
         
      }))

      refreshAlert.addAction(UIAlertAction(title: "Delete Notification", style: .default, handler: { (action: UIAlertAction!) in
         print("Delete")
         UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [myName])
         UserDefaults.standard.removeObject(forKey: myName)
         print("after", UserDefaults.standard.bool(forKey: myName))
         self.loadScreen()
      }))
      present(refreshAlert, animated: true, completion: nil)
   }

   @IBAction func lblValue2Click(_ sender: Any) {
      let myName = "DailyTime2"
      // Prompt to Delete or change
      let refreshAlert = UIAlertController(title: "Modify Notification", message: "Would you like to Modify or Delete the Notification?", preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action:UIAlertAction) in
      }))
      
      refreshAlert.addAction(UIAlertAction(title: "Modify Notification", style: .default, handler: { (action: UIAlertAction!) in
         self.timeSelector(myNotification: myName)
      }))

      refreshAlert.addAction(UIAlertAction(title: "Delete Notification", style: .default, handler: { (action: UIAlertAction!) in
         print("Delete")
         UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [myName])
         UserDefaults.standard.removeObject(forKey: myName)
         self.loadScreen()
      }))
      present(refreshAlert, animated: true, completion: nil)
   }
   @IBAction func lblValue3Click(_ sender: Any) {
      let myName = "DailyTime3"
      // Prompt to Delete or change
      let refreshAlert = UIAlertController(title: "Modify Notification", message: "Would you like to Modify or Delete the Notification?", preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action:UIAlertAction) in
      }))
      
      refreshAlert.addAction(UIAlertAction(title: "Modify Notification", style: .default, handler: { (action: UIAlertAction!) in
         self.timeSelector(myNotification: myName)
         
      }))

      refreshAlert.addAction(UIAlertAction(title: "Delete Notification", style: .default, handler: { (action: UIAlertAction!) in
         UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [myName])
         UserDefaults.standard.removeObject(forKey: myName)
         self.loadScreen()
      }))
      present(refreshAlert, animated: true, completion: nil)
   }
   
   @IBAction func lblValue4Click(_ sender: Any) {
      let myName = "DailyTime4"
      // Prompt to Delete or change
      let refreshAlert = UIAlertController(title: "Modify Notification", message: "Would you like to Modify or Delete the Notification?", preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action:UIAlertAction) in
      }))
      
      refreshAlert.addAction(UIAlertAction(title: "Modify Notification", style: .default, handler: { (action: UIAlertAction!) in
         self.timeSelector(myNotification: myName)
         
      }))

      refreshAlert.addAction(UIAlertAction(title: "Delete Notification", style: .default, handler: { (action: UIAlertAction!) in
         UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [myName])
         UserDefaults.standard.removeObject(forKey: myName)
         self.loadScreen()
      }))
      present(refreshAlert, animated: true, completion: nil)
   }
   
   @IBAction func lblValue5Click(_ sender: Any) {
      let myName = "DailyTime5"
      // Prompt to Delete or change
      let refreshAlert = UIAlertController(title: "Modify Notification", message: "Would you like to Modify or Delete the Notification?", preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action:UIAlertAction) in
      }))
      
      refreshAlert.addAction(UIAlertAction(title: "Modify Notification", style: .default, handler: { (action: UIAlertAction!) in
         self.timeSelector(myNotification: myName)
         
      }))

      refreshAlert.addAction(UIAlertAction(title: "Delete Notification", style: .default, handler: { (action: UIAlertAction!) in
         UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [myName])
         UserDefaults.standard.removeObject(forKey: myName)
         self.loadScreen()
      }))
      present(refreshAlert, animated: true, completion: nil)
   }
   
   


}
