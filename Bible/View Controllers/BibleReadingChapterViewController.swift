//
//  BibleReadingChapterViewController.swift
//  Bible
//
//  Created by Douglas Palme on 7/19/21.
//

import UIKit

class BibleReadingChapterViewController: UIViewController {

   var ChapterCt: String = ""
   var key: String = ""
   var BibleBook: String = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
      self.title = BibleBook + " Chapter Selection"
        // Do any additional setup after loading the view.
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if segue.identifier == "BibleReadingChapterTableViewController" {
         if let BibleReadingChapterTableViewController = segue.destination  as? BibleReadingChapterTableViewController {
            BibleReadingChapterTableViewController.ChapterCt = ChapterCt
            BibleReadingChapterTableViewController.key = key
            BibleReadingChapterTableViewController.BibleBook = BibleBook
         }
      }
   }

}
