//
//  BibleReadingTableViewController.swift
//  Bible
//
//  Created by Douglas Palme on 7/19/21.
//

import UIKit
import Firebase
import FirebaseDatabase

class BibleReadingTableViewController: UITableViewController {

   let myRef = Database.database()
   public var items: [BkItem] = []

   
    override func viewDidLoad() {
        super.viewDidLoad()

      let myRef = myRef.reference(withPath: "Books")
      myRef.keepSynced(true)
      // observe value of reference
      myRef.observe(.value, with: {
         snapshot in
         var newItems: [BkItem] = []
         for item in snapshot.children {
            let mItem = BkItem(snapshot: item as! DataSnapshot)
            newItems.append(mItem)
         }
         self.items = newItems
         self.tableView.reloadData()
      })
      
    }

   // MARK: - Table view data source
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.items.count
   }

   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
      cell.layer.cornerRadius = 8
      cell.layer.backgroundColor = UIColor.clear.cgColor
      let myView = cell.viewWithTag(1)
      let label1 = cell.viewWithTag(2) as? UILabel
      let label2 = cell.viewWithTag(3) as? UILabel
      let label3 = cell.viewWithTag(4) as? UILabel
      let myItem = self.items[indexPath.row]
      myView!.layer.shadowColor = UIColor.black.cgColor
      myView!.layer.shadowOffset = CGSize(width: 5, height: 5)
      myView!.layer.shadowRadius = 5
      myView!.layer.shadowOpacity = 1.0
      myView?.layer.borderWidth = 1
      myView?.layer.borderColor = UIColor.gray.cgColor
      label1?.text = myItem.nm
      label2?.text = String(myItem.ch)
      label3?.text = String(myItem.key)
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      if let cell = tableView.cellForRow(at: indexPath) {
         let label1 = cell.viewWithTag(2) as? UILabel
         let label2 = cell.viewWithTag(3) as? UILabel
         let label3 = cell.viewWithTag(4) as? UILabel
         let myVC = storyboard?.instantiateViewController(withIdentifier: "BibleReadingChapterViewController") as! BibleReadingChapterViewController
         myVC.ChapterCt = label2?.text ?? "0"
         myVC.key = label3?.text ?? "0"
         myVC.BibleBook = label1?.text ?? ""
         navigationController?.pushViewController(myVC, animated: true)
         }
      }
   
}
   
   

