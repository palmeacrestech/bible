import UIKit
import FirebaseDatabase

class BrPageViewController: UIViewController {
   //1
   var pageIndex: Int = 0
   var maxCount: Int = 0
   public var VerseString: String = ""
   public var PassageString: String = ""
   var zVerse: String = ""
   var zPassage: String = ""
   let dbRef = Database.database()

   var favorites = Favorites()
  
   @IBOutlet weak var pageIndexLabel: UILabel!
   
   @IBOutlet weak var pageImageView: UIImageView!
   @IBOutlet weak var pageContentTextView: UITextView!
   @IBOutlet weak var lblVerse: UILabel!
   @IBOutlet weak var lblKey: UILabel!
   @IBOutlet weak var btnFav: UIButton!

   override func viewDidLoad() {
      super.viewDidLoad()
     
         // turn off back button
      self.navigationItem.setHidesBackButton(true, animated: false)
   }

   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated);
      self.navigationController?.setToolbarHidden(true, animated: animated)
      self.navigationController?.navigationBar.isTranslucent = true
   }
   
   override func viewWillLayoutSubviews() {
      self.navigationController?.setNavigationBarHidden(true, animated: true)

       self.navigationController?.isNavigationBarHidden = true
      self.navigationController?.hidesBottomBarWhenPushed = true
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.navigationController?.navigationItem.hidesBackButton = true
      self.navigationController?.setNavigationBarHidden(true, animated: true)
      let myRef2 = dbRef.reference().child("Verses1")
      myRef2.keepSynced(true)
      myRef2.child(String(pageIndex)).observeSingleEvent(of: .value, with: { [self] (snapshot) in
          let value = snapshot.value as? NSDictionary
          let oVerse = value?["V"] as? String ?? ""
          let oPassage = value?["P"] as? String ?? ""

         self.lblKey.text = oVerse
         self.lblVerse.text = oPassage
         
      }) { (error) in
          print(error.localizedDescription)
      }

      self.navigationController?.setToolbarHidden(false, animated: animated)
      self.lblVerse.fadeOut()
      self.lblKey.fadeOut()
      self.lblKey.textAlignment = .center
      self.lblKey.text = PassageString
      
      self.lblVerse.textAlignment = .justified
      self.lblVerse.text = VerseString
      self.lblVerse.fadeIn()
      self.lblKey.fadeKey()
      //self.pageIndexLabel.text = "Page Index: \(pageIndex)"
      // check to see if verse is a favorite
      if self.favorites.contains(myValue: pageIndex) == true {
         // we are going to change the image to a full heart
         self.btnFav.setImage(UIImage(named: "heart3"), for: .normal)
      } else {
         self.btnFav.setImage(UIImage(named: "heart2"), for: .normal)
      }
      pageImageView.image = UIImage(named: "\(arc4random_uniform(50) + 1).png")
   }
   
   @IBAction func btnAddRemove(_ sender: Any) {
      let retVal = favorites.addRemove(myValue: pageIndex)
      if retVal == "Add" {
         self.btnFav.setImage(UIImage(named:"heart3"), for: .normal)
      } else {
         self.btnFav.setImage(UIImage(named: "heart2"), for: .normal)
      }
   }
   
   @IBAction func btnShare(_ sender: Any) {
      UIApplication.shared.windows.first?.tintColor = .black
      //UIApplication.shared.keyWindow?.tintColor = .black
      let myHeader = "I thought of you when I read this verse, I hope and pray that you find it as uplifting as I did:\n\n"
      let myString = myHeader + "\"" + lblVerse.text! + "\"" + " " + lblKey.text!
      let urlStr = "https://apps.apple.com/app/id1566625562"
      let myObj = myString + "\n\n" + urlStr
      let objectsToShare = [myObj]
      let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
      
         if UIDevice.current.userInterfaceIdiom == .pad {
              if let popup = activityVC.popoverPresentationController {
                  popup.sourceView = self.view
                  popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
              }
          }
          self.present(activityVC, animated: true, completion: nil)
   }
   
   @IBAction func btnHamburgerPressed(_ sender: Any) {
      self.navigationController?.navigationBar.isHidden = false
      //guard let myVC1 = self.storyboard?.instantiateViewController(withIdentifier: "Menu1ViewController") as?
      //         Menu1ViewController else { return }
      let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "Menu1ViewController") as! Menu1ViewController
      self.navigationController?.pushViewController(newViewController, animated: true)
      //self.navigationController?.pushViewController(myVC1, animated: true)
   }
}

