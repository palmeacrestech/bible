//
//  StudyViewController.swift
//  Bible
//
//  Created by Douglas Palme on 7/16/21.
//

import UIKit

class StudyViewController: UIViewController {

   override func viewDidLoad() {
       super.viewDidLoad()
     self.navigationController?.setNavigationBarHidden(false, animated: true)
     self.navigationController!.navigationBar.tintColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
     _ = UIBarButtonItemAppearance()
   }
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
