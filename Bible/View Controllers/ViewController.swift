//
//  ViewController.swift
//  PageViewController-Demo
//
//  Created by Shankar B S on 14/03/20.
//  Copyright © 2020 Slicode. All rights reserved.
//

import UIKit
import ZendeskCoreSDK
import SupportSDK

class ViewController: UIViewController {

   var maxCount: Int = 0
   var myIndex: Int =  0
   var RunOnce: Bool = false
   let Reviews = Review()
   var Book: Int = 0
   var Chapter: Int = 0
   public var inParm3: String = ""
   public var BibleBook: String = ""
   let myFunc = Functions()

   
    @IBOutlet weak var pageControllerHolderView: UIView!
   
    lazy var pageViewController: UIPageViewController = {
      return UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
    }()
    
   override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.navigationItem.hidesBackButton = true
      self.navigationController?.setNavigationBarHidden(true, animated: true)
   }
   
   override func viewWillLayoutSubviews() {
      self.navigationController?.setNavigationBarHidden(true, animated: true)
       self.navigationController?.isNavigationBarHidden = true
   }
   
    override func viewDidLoad() {
        super.viewDidLoad()
         if inParm3 != "" {
         } else {
            if RunOnce == false {
               RunOnce = true
               if myIndex == 0 {
                  myIndex = myFunc.getDayOfYear()
               }
            }
         }
      self.navigationItem.setHidesBackButton(true, animated: false)
      setupPageViewController()
   }

   func setupPageViewController() {
      
      //set its datasource and delegate methods
      self.pageViewController.dataSource = self
      self.pageViewController.delegate = self
      //self.pageViewController.view.frame = .zero
      // Get firebase Data
      //Show view controller with initial page - page zero
      
      let pageController = getPageFor(index: myIndex)
      guard let initialPageController = pageController else { return }
      
      self.navigationController?.navigationItem.hidesBackButton = true

      
      pageController?.inParm3 = self.inParm3
      pageController?.Book = self.Book
      pageController?.Chapter = self.Chapter
      pageController?.BibleBook = self.BibleBook
      self.pageViewController.setViewControllers([initialPageController], direction: .forward, animated: false, completion: nil)
      
      self.addChild(self.pageViewController)
      
      //Add to holder view
      self.pageControllerHolderView.addSubview(self.pageViewController.view)
      self.pageViewController.didMove(toParent: self)
      
      //Pin to super view - (holder view)
      self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
      self.pageViewController.view.topAnchor.constraint(equalTo: self.pageControllerHolderView.topAnchor).isActive = true
      self.pageViewController.view.leftAnchor.constraint(equalTo: self.pageControllerHolderView.leftAnchor).isActive = true
      self.pageViewController.view.bottomAnchor.constraint(equalTo: self.pageControllerHolderView.bottomAnchor).isActive = true
      self.pageViewController.view.rightAnchor.constraint(equalTo: self.pageControllerHolderView.rightAnchor).isActive = true

      //navigationController?.hidesBarsOnTap = true
      
   }

    //Helper method to create view controllers for page view controller
    //for specified index
    func getPageFor(index: Int) -> PageViewController? {
        guard  let pageController  = self.storyboard?.instantiateViewController(identifier: "PageViewController") as? PageViewController else { return nil }
        pageController.pageIndex = index
         pageController.inParm3 = self.inParm3
         pageController.Book = self.Book
         pageController.Chapter = self.Chapter
      pageController.BibleBook = self.BibleBook
      return pageController
    }
   
   func wrtReview() {
      let refreshAlert = UIAlertController(title: "Write A Review", message: "Would you like to leave us a review?", preferredStyle: UIAlertController.Style.alert)
      refreshAlert.addAction(UIAlertAction(title: "5 Star Rating", style: .default, handler: { (action: UIAlertAction!) in
         if let url = URL(string: "https://apps.apple.com/app/id1566625562") {
            UIApplication.shared.open(url)
            // eliminate prompt
            UserDefaults.standard.setValue(1, forKey: "Reviewed")
         }
      }))
      refreshAlert.addAction(UIAlertAction(title: "1-4 Star Rating", style: .cancel, handler: { (action: UIAlertAction!) in
         let refreshAlert1 = UIAlertController(title: "Talk to Support", message: "Would you like to discuss any problems or issues with our support team?", preferredStyle: UIAlertController.Style.alert)
         refreshAlert1.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.navigationItem.hidesBackButton = false
            self.navigationController?.isNavigationBarHidden = false
            let helpCenter1 = ZDKHelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [])
            self.navigationController?.pushViewController(helpCenter1, animated: true)
         }))
         refreshAlert1.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Do nothing")
         }))
         self.present(refreshAlert1, animated: true, completion: nil)
      }))
      present(refreshAlert, animated: true, completion: nil)
   }
}

extension ViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore
        viewController: UIViewController) -> UIViewController? {
        guard let beforePage = viewController as? PageViewController else { return nil }
        let beforePageIndex = beforePage.pageIndex
        //since it is before we need to go back the index
        let newIndex = beforePageIndex - 1
        if newIndex < 0 { return nil }
        return getPageFor(index: newIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let afterPage = viewController as? PageViewController else { return nil }
         let afterPageIndex = afterPage.pageIndex
        //since it is after we need to go forword
   
      let newIndex = afterPageIndex + 1
      
         // Check if this is a reading section
      if inParm3 == "Reading" {
         if newIndex < 0 || newIndex > 366 { return nil }
         return getPageFor(index: newIndex)
      } else {
        if newIndex < 0 || newIndex > 366 { return nil }
        return getPageFor(index: newIndex)
      }
    }

   func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
      // check to see if we should present the review?
      if UserDefaults.standard.value(forKey: "Reviewed") as! Int != 1 {
         Review.UpdateCt()
         if Review.checkCt() == true  {
            wrtReview()
         }
      }
   }

   
}

