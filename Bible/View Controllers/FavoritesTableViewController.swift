//
//  FavoritesTableViewController.swift
//  Bible
//
//  Created by Douglas Palme on 6/16/21.
//

import UIKit
import Firebase
import FirebaseDatabase

class FavoritesTableViewController: UITableViewController {
   
   let defaults = UserDefaults.standard
   var intArray = [Int]()
   let favorites = Favorites()
   
   public var dbParm: String = ""
   public var myCategory: String = ""
   public var items: [FavItem] = []
   let myRef = Database.database()
   public var items1: [FavItem] = []
   
   @IBOutlet var table: UITableView!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      self.title = "Favorites"
      table.backgroundView = UIImageView(image: UIImage(named: "bible1"))
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      self.navigationController!.navigationBar.tintColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
      
      
      intArray = defaults.array(forKey: "Favorites") as? [Int] ?? []
      if intArray.count > 0 {
         let myCount = intArray.count
         for index in 1...myCount {
            //myCategory = "Test/D"
            dbParm = "Verses1/" + String(intArray[index - 1])
            print(dbParm)
            let myRef = myRef.reference(withPath: dbParm)
            myRef.keepSynced(true)
            // observe value of reference
            myRef.observe(.value, with: {
               snapshot in
               //var newItems1: [FavItem] = []
               let mItem = FavItem(snapshot: snapshot)
               self.items1.append(mItem)
               print(self.items1)
               self.tableView.reloadData()
            })
         }
      }
   }
   
   
   // MARK: - Table view data source
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      // #warning Incomplete implementation, return the number of rows
      return self.items1.count
   }
   
   
   
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
      print(self.items1)
      // Configure the cell...
      cell.layer.cornerRadius = 8
      let label1 = cell.viewWithTag(1) as? UILabel
      let label2 = cell.viewWithTag(2) as? UILabel
      let label4 = cell.viewWithTag(4) as? UILabel
      let myView = cell.viewWithTag(3)
      myView!.layer.shadowColor = UIColor.black.cgColor
      myView!.layer.shadowOffset = CGSize(width: 5, height: 5)
      myView!.layer.shadowRadius = 5
      myView!.layer.shadowOpacity = 1.0
      myView?.clipsToBounds = true
      //myView?.layer.borderWidth = 1
      //myView?.layer.borderColor = UIColor.gray.cgColor
      let myItem = self.items1[indexPath.row]
      label1?.text = myItem.Verse
      label2?.text = myItem.Passage
      label4?.text = String(myItem.Key)
      
      
      table.rowHeight = UITableView.automaticDimension
      table.estimatedRowHeight = 600
      
      return cell
   }
   
   override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
         let currentCell = tableView.cellForRow(at: indexPath)!
         let label4 = currentCell.viewWithTag(4) as? UILabel
         let myInt = Int((label4?.text)!)!
         // remove value from intArray
         if favorites.addRemove(myValue: myInt) == "Removed" {
            // remove items from items1
            for myKey in 0...(items1.count) {
               if items1[myKey].Key == myInt {
                  print(myInt)
                  print(items1[myKey].Key)
                  print("we remove this item")
                  items1.remove(at: myKey)
                  break
               } else {
                  print("this item stays")
               }
            }
            // now we remove it from the screen
            print("After: \(tableView.numberOfRows(inSection: 0))")
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            print(tableView.numberOfRows(inSection: 0))
            tableView.reloadData()
         }
         
      }
   }
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      if let cell = tableView.cellForRow(at: indexPath) {
         let label4 = cell.viewWithTag(4) as? UILabel
         // pass value to View Controller for display
         let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
         let myString = (label4?.text)!
         let myInt = Int(myString)
         myVC.myIndex = myInt!
         navigationController?.pushViewController(myVC, animated: true)
      }
   }
   
}
