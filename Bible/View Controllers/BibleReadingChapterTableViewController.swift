//
//  BibleReadingChapterTableViewController.swift
//  Bible
//
//  Created by Douglas Palme on 7/19/21.
//

import UIKit

class BibleReadingChapterTableViewController: UITableViewController {

   var ChapterCt: String = ""
   var key: String = ""
   var intArray = [String]()
   var BibleBook : String = ""
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
      // populate array
      for n in 1...(Int(ChapterCt) ?? 0) {
         let myString = "Chapter \(n)"
         intArray.append(myString)
      }
      
    }

   // MARK: - Table view data source
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.intArray.count
   }

   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
      cell.layer.cornerRadius = 8
      cell.layer.backgroundColor = UIColor.clear.cgColor
      let myView = cell.viewWithTag(1)
      let label1 = cell.viewWithTag(2) as? UILabel
      let label2 = cell.viewWithTag(3) as? UILabel
      
      let myItem = self.intArray[indexPath.row]
      let stringArray = myItem.components(separatedBy: CharacterSet.decimalDigits.inverted)
      for nItem in stringArray {
          if let number = Int(nItem) {
            label2?.text = String(number)
          }
      }
      myView!.layer.shadowColor = UIColor.black.cgColor
      myView!.layer.shadowOffset = CGSize(width: 5, height: 5)
      myView!.layer.shadowRadius = 5
      myView!.layer.shadowOpacity = 1.0
      myView?.layer.borderWidth = 1
      myView?.layer.borderColor = UIColor.gray.cgColor
      label1?.text = myItem
      return cell
   }
  
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      if let cell = tableView.cellForRow(at: indexPath) {
         let label1 = cell.viewWithTag(2) as? UILabel
         let label2 = cell.viewWithTag(3) as? UILabel
         // let label3 = cell.viewWithTag(4) as? UILabel
         

         let myChapter = Int(label2?.text ?? "0")
         let myBook = Int(key)
         let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
         myVC.Chapter = (myChapter ?? 0) - 1
         myVC.Book = myBook ?? 0
         myVC.inParm3 = "Reading"
         myVC.BibleBook = BibleBook
         navigationController?.pushViewController(myVC, animated: true)
         }
      }
}
