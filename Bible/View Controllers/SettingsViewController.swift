//
//  SettingsViewController.swift
//  Bible
//
//  Created by Douglas Palme on 4/22/21.
//

import UIKit
import AVFoundation

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

   @IBOutlet weak var swMusic: UISwitch!
   @IBOutlet weak var musicSelection: UIPickerView!
   
   var pickerData: [String] = [String]()
   var pickerSelected: String = ""
   


   
   
   override func viewDidLoad() {
        super.viewDidLoad()
      self.musicSelection.delegate = self
      self.musicSelection.dataSource = self
      //print(UserDefaults.standard.bool(forKey: "isPlaying"))
      swMusic.isOn = UserDefaults.standard.bool(forKey: "isPlaying")
      //var myComponent: Int = 0
      pickerData = ["Reach", "Beyond-The-Summit"]
      // Print
      //print(pickerData)
      // get default value from
      let defaultItem = UserDefaults.standard.value(forKey: "musicTrack")
      //print(defaultItem)
      //if defaultItem as! String == "Reach" {
      //   myComponent = 0
      //} else {
      //   myComponent = 1
      //}
      
      setDefaultValue(item: defaultItem as! String, inComponent: 0)
   
      musicSelection.layer.borderColor = UIColor.black.cgColor
      musicSelection.layer.borderWidth = 1
      musicSelection.layer.cornerRadius = 40
   }

   func setDefaultValue(item: String, inComponent: Int){
    if let indexPosition = pickerData.firstIndex(of: item){
      print(indexPosition)
      musicSelection.selectRow(indexPosition, inComponent: inComponent, animated: true)
    }
   }
   
   @IBAction func musicSwitch(_ sender: Any) {
      if swMusic.isOn == true {
         // turn on music
         // get default
         Music.StartPlaying(musicName: UserDefaults.standard.value(forKey: "musicTrack") as! String)
      } else {
         Music.StopPlaying()
      }
   }

   func numberOfComponents(in musicSelection: UIPickerView) -> Int {
      return 1
   }
   
   
   func pickerView(_ musicSelection: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      return pickerData.count
   }

   // The data to return for the row and component (column) that's being passed in
       func pickerView(_ musicSelection: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return pickerData[row]
       }
   
   func pickerView(_ musicSelection: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      pickerSelected = pickerData[row]
      // write it to the user default if it is different.
      if pickerSelected != UserDefaults.standard.value(forKey: "musicTrack") as! String {
         // change it
         UserDefaults.standard.setValue(pickerSelected, forKey: "musicTrack")
         // call music
         if swMusic.isOn == true {
            Music.StartPlaying(musicName: pickerSelected)
         }
      }
   }
   
   
   
   
}
