//
//  SubscriptionViewController.swift
//  Bible
//
//  Created by Douglas Palme on 4/21/21.
//

import UIKit
import Purchases

class SubscriptionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   
   

   @IBOutlet weak var txtView: UITextView!
   @IBOutlet weak var pickerView: UIPickerView!
   @IBOutlet weak var btnSubscribeTitle: MyButton!
   @IBOutlet weak var activityView: UIActivityIndicatorView!
   
   var pickerData: [String] = [String]()
   var pickerSelected: String = ""
   var myProduct: String = "com.palmeacres.bible1."
   
   override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.navigationItem.hidesBackButton = true
      activityView.style = .large
      activityView.color = .black
      // check if we are already subscribed
      if UserDefaults.standard.value(forKey: "isSubscribed") as! Int == 1 {
         btnSubscribeTitle.setTitle("Subscribed", for: .normal)
         btnSubscribeTitle.isEnabled = false
      } else {
         btnSubscribeTitle.setTitle("Try Free & Subscribe", for: .normal)
         btnSubscribeTitle.isEnabled = true
      }
   }
   

    override func viewDidLoad() {
        super.viewDidLoad()

      self.navigationItem.setHidesBackButton(true, animated: false)
      
      self.pickerView.delegate = self
      self.pickerView.dataSource = self
      
      txtView.text = "Welcome! We would humbly suggest a contribution of $29.99/yr or feel free to choose a different amount.  contributor, you will receive all app features."
    
      pickerData = ["$99.99 / Year", "$59.99 / Year", "$39.99 / Year", "$29.99 / Year", "$19.99 / Year", "$9.99 / Month", "$5.99 / Month", "$4.99 / Month", "$3.99 / Month", "$2.99 / Month", "$1.99 / Month"]

      pickerView.layer.borderColor = UIColor.black.cgColor
      pickerView.layer.borderWidth = 1
      pickerView.layer.cornerRadius = 40
    
    }
    
   
   @IBAction func btnClose(_ sender: Any) {
      if UserDefaults.standard.integer(forKey: "firstRun") == 1 {
         guard let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return }
         navigationController?.pushViewController(myVC, animated: true)
      } else {
         navigationController?.popViewController(animated: true)
      }
   }
   
   func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 1
   }
   
   
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      return pickerData.count
   }

   // The data to return fopr the row and component (column) that's being passed in
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return pickerData[row]
       }
   
   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      pickerSelected = pickerData[row]
   }
   
   @IBAction func btnSubscribe(_ sender: Any) {
      if pickerSelected == "" {
         myProduct = "com.palmeacres.bible1.99.99"
      } else {
         switch pickerSelected {
         case "$99.99 / Year":
            myProduct = "com.palmeacres.bible1.99.99"
         case "$59.99 / Year":
            myProduct = "com.palmeacres.bible1.59.99"
         case "$39.99 / Year":
            myProduct = "com.palmeacres.bible1.39.99"
         case "$29.99 / Year":
            myProduct = "com.palmeacres.bible1.29.99"
         case "$19.99 / Year":
            myProduct = "com.palmeacres.bible1.19.99"
         case "$9.99 / Month":
            myProduct = "com.palmeacres.bible1.9.99"
         case "$5.99 / Month":
            myProduct = "com.palmeacres.bible1.5.99"
         case "$4.99 / Month":
            myProduct = "com.palmeacres.bible1.4.99"
         case "$3.99 / Month":
            myProduct = "com.palmeacres.bible1.3.99"
         case "$2.99 / Month":
            myProduct = "com.palmeacres.bible1.2.99"
         case "$1.99 / Month":
            myProduct = "com.palmeacres.bible1.1.99"
         case "Request a Scholarship":
            pickerSelected = "0.00"
            // need special handler for this
         default:
            myProduct = "com.palmeacres.bible1.99.99"
         }
      }
      
      print(myProduct)
      Subscription(myProduct: myProduct)
      //Subscriptions(myProduct: myProduct)
   }
   
   func Subscription(myProduct: String) {
      Purchases.shared.offerings { (offerings, error) in
         if let e = error {
             print(e.localizedDescription)
         }
         Purchases.shared.offerings { (offerings, error) in
             if let packages = offerings?.current?.availablePackages {
                 // match up package to selection
               // get count
               for package in packages {
                  print(package.identifier)
                  if package.identifier == myProduct {
                     print("We have a Weiner")
                     Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, userCancelled) in
                        if userCancelled == true {
                           print("User Canceled")
                           UserDefaults.standard.setValue("", forKey: "PurType")
                           return
                        } else if error != nil {
                           print(error?.localizedDescription ?? "Error")
                           return
                        } else if purchaserInfo?.entitlements["Monthly"]?.isActive == true {
                           print("Monthly")
                           UserDefaults.standard.setValue("", forKey: "PurType")
                           UserDefaults.standard.setValue(1, forKey: "isSubscribed")
                        } else if purchaserInfo?.entitlements["Yearly"]?.isActive == true {
                           print("Yearly")
                           // update userdefined
                           UserDefaults.standard.set("Yearly", forKey: "PurType")
                           UserDefaults.standard.setValue(1, forKey: "isSubscribed")
                        }
                     }
                  }
               }

             }
         }
      }
   }
   
   
   
   
}
