//
//  MenuTableViewController.swift
//  Bible
//
//  Created by Douglas Palme on 7/8/21.
//

import UIKit
import Firebase
import FirebaseDatabase
import Purchases
import SupportSDK
import ZendeskCoreSDK

class MenuTableViewController: UITableViewController, UIActivityItemSource {
   

   
   @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
   public var items: [MenuItem] = []
   public var myItemCount: Int = 0
   public var myString: String = ""
   let myRef = Database.database()
   public var subMenu: String = "Menu"

   override func viewDidLoad() {
      super.viewDidLoad()

#if targetEnvironment(simulator)
      let isSim: Bool = true
#else
      let isSim: Bool = false
#endif

      let myRef = myRef.reference(withPath: subMenu)
      myRef.keepSynced(true)
      // observe value of reference
      myRef.observe(.value, with: {
         snapshot in
         var newItems: [MenuItem] = []
         for item in snapshot.children {
            let mItem = MenuItem(snapshot: item as! DataSnapshot)


            if mItem.act == "Y" || (mItem.act == "N" && isSim == true) {
               newItems.append(mItem)
            }
         }
         self.items = newItems
         self.items.sort(by: {$0.key < $1.key})
         newItems = self.items
         self.tableView.reloadData()
      })
   }
   
   // MARK: - Table view data source
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return items.count
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
      cell.layer.cornerRadius = 8
      cell.layer.backgroundColor = UIColor.clear.cgColor
      //let label1 = cell.viewWithTag(1) as? UILabel
      //let label2 = cell.viewWithTag(2) as? UILabel
      //let label3 = cell.viewWithTag(4) as? UILabel
      //let label6 = cell.viewWithTag(6) as? UILabel
      let myView = cell.viewWithTag(1)
      let label1 = cell.viewWithTag(2) as? UILabel
      let myItem = items[indexPath.row]
      myView!.layer.shadowColor = UIColor.black.cgColor
      myView!.layer.shadowOffset = CGSize(width: 5, height: 5)
      myView!.layer.shadowRadius = 5
      myView!.layer.shadowOpacity = 1.0
      
      myView?.layer.borderWidth = 1
      myView?.layer.borderColor = UIColor.gray.cgColor
      label1?.text = myItem.nm
      
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      if let cell = tableView.cellForRow(at: indexPath) {
         let label = cell.viewWithTag(2) as? UILabel
         switch label?.text {
         
         case "Bible Study":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "StudyViewController") as! StudyViewController
            navigationController?.pushViewController(myVC, animated: true)
            
         case "Settings":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            navigationController?.pushViewController(myVC, animated: true)
            
         case "Bible Reading":
            if UserDefaults.standard.integer(forKey: "isSubscribed") == 0 {
               // go to subscription screen
               guard let myVC = storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as? SubscriptionViewController else { return }
               navigationController?.pushViewController(myVC, animated: true)
            } else {
               let myVC = storyboard?.instantiateViewController(withIdentifier: "BibleReadingViewController") as! BibleReadingViewController
               navigationController?.pushViewController(myVC, animated: true)
            }
         case "Restore Purchases":
            let activityView = UIActivityIndicatorView()
            activityView.style = .large
            activityView.color = .black
            activityView.center = self.view.center
            self.view.addSubview(activityView)
            activityView.startAnimating()
            Purchases.shared.restoreTransactions { (purchaserInfo, error) in
               //... check purchaserInfo to see if entitlement is now active
               if let err = error {
                  activityView.stopAnimating()
                  print(err.localizedDescription)
                  self.displayError(error: err.localizedDescription)
               }
               if purchaserInfo?.entitlements["Monthly"]?.isActive == true {
                  UserDefaults.standard.setValue(1, forKey: "isSubscribed")
                  UserDefaults.standard.setValue("Monthly", forKey: "PurType")
                  activityView.stopAnimating()
                  self.displayActive()
               } else if purchaserInfo?.entitlements["Yearly"]?.isActive == true {
                  UserDefaults.standard.setValue(1, forKey: "isSubscribed")
                  UserDefaults.standard.setValue("Yearly", forKey: "PurType")
                  activityView.stopAnimating()
                  self.displayActive()
               } else {
                  UserDefaults.standard.setValue(0, forKey: "isSubscribed")
                  UserDefaults.standard.setValue("", forKey: "PurType")
                  activityView.stopAnimating()
                  self.displayNotActive()
               }
            }
         case "Invite a Friend":
            UIApplication.shared.windows.first?.tintColor = .black
            
            //UIApplication.shared.keyWindow?.tintColor = .black
            if let urlStr = NSURL(string: "https://apps.apple.com/app/id1566625562") {
               let objectsToShare = [urlStr]
               let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
               
               if UIDevice.current.userInterfaceIdiom == .pad {
                  if let popup = activityVC.popoverPresentationController {
                     popup.sourceView = self.view
                     popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                  }
               }
               
               self.present(activityVC, animated: true, completion: nil)
            }
         case "Become a Patron":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
            navigationController?.pushViewController(myVC, animated: true)
            
         case "Contact Support":
            self.navigationController?.navigationItem.hidesBackButton = false
            self.navigationController?.isNavigationBarHidden = false
            let helpCenter1 = ZDKHelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [])
            self.navigationController?.pushViewController(helpCenter1, animated: true)
            
         case "Write a Review":
            let refreshAlert = UIAlertController(title: "Write A Review", message: "Would you like to leave us a review?", preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: "5 Star Rating", style: .default, handler: { (action: UIAlertAction!) in
               if let url = URL(string: "https://apps.apple.com/app/id1566625562") {
                  UIApplication.shared.open(url)
               }
            }))
            refreshAlert.addAction(UIAlertAction(title: "1-4 Star Rating", style: .cancel, handler: { (action: UIAlertAction!) in
               let refreshAlert1 = UIAlertController(title: "Talk to Support", message: "Would you like to discuss any problems or issues with our support team?", preferredStyle: UIAlertController.Style.alert)
               refreshAlert1.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                  self.navigationController?.navigationItem.hidesBackButton = false
                  self.navigationController?.isNavigationBarHidden = false
                  let helpCenter1 = ZDKHelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [])
                  self.navigationController?.pushViewController(helpCenter1, animated: true)
               }))
               refreshAlert1.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
                  print("Do nothing")
               }))
               self.present(refreshAlert1, animated: true, completion: nil)
            }))
            present(refreshAlert, animated: true, completion: nil)
            
         case "Privacy Policy":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            navigationController?.pushViewController(myVC, animated: true)
            
         case "Notifications":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            navigationController?.pushViewController(myVC, animated: true)
            
         case "Favorites":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "FavoritesTableViewController") as! FavoritesTableViewController
            navigationController?.pushViewController(myVC, animated: true)
            
         case "Terms of Service":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
            navigationController?.pushViewController(myVC, animated: true)
         case "Bible Study":
            let myVC = storyboard?.instantiateViewController(withIdentifier: "Dougie") as! StudyViewController
            navigationController?.pushViewController(myVC, animated: true)
         default:
            print("Do Nothing")
         }
      }
   }
   
   func displayActive() {
      // display message
      let message = "We have restored your purchase."
      let myTitle = "Restore Purchase"
      
      let alert = UIAlertController(title: myTitle,
                                    message: message,
                                    preferredStyle: .alert)
      let action = UIAlertAction(title: "OK", style: .default, handler: nil)
      alert.addAction(action)
      self.present(alert, animated: true, completion: nil)
   }
   func displayNotActive() {
      // display message
      let message = "We could not locate a valid subscription for you at this time, if you believe this is in error, please contact our support team for assistance."
      let myTitle = "Restore Purchase"
      
      let alert = UIAlertController(title: myTitle,
                                    message: message,
                                    preferredStyle: .alert)
      let action = UIAlertAction(title: "OK", style: .default, handler: nil)
      alert.addAction(action)
      self.present(alert, animated: true, completion: nil)
   }
   
   func displayMsg(Msg: String, Title: String) {
      let message = Msg
      let myTitle = Title
      let alert = UIAlertController(title: myTitle,
                                    message: message,
                                    preferredStyle: .alert)
      let action = UIAlertAction(title: "OK", style: .default)
      alert.addAction(action)
      self.present(alert, animated: true, completion: nil)
   }
   
   
   func displayError(error: String) {
      // display message
      let message = "An error was encountered while attempting to restore your latest purchase, please contact our support team and provide them with the following information: \(error)."
      let myTitle = "Restore Purchase"
      
      let alert = UIAlertController(title: myTitle,
                                    message: message,
                                    preferredStyle: .alert)
      let action = UIAlertAction(title: "OK", style: .default, handler: nil)
      alert.addAction(action)
      self.present(alert, animated: true, completion: nil)
   }
   
   func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
      return "The pig is in the poke"
   }
   
   func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
      return "The pig is in the poke"
   }
}



