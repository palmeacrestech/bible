//
//  AppDelegate.swift
//  PageViewController-Demo
//
//  Created by Shankar B S on 14/03/20.
//  Copyright © 2020 Slicode. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import SupportSDK
import ZendeskCoreSDK
import Purchases
import FirebaseAnalytics
import FirebaseDatabase
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      
      
      
      // Override point for customization after application launch.
      FirebaseApp.configure()
      
      Analytics.setAnalyticsCollectionEnabled(true)

      // Override point for customization after application launch.
      // Sets background to a blank/empty image
      UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
      
      // Sets shadow (line below the bar) to a blank image
      UINavigationBar.appearance().shadowImage = UIImage()
      
      // Sets the translucent background color
      UINavigationBar.appearance().backgroundColor = .clear
      
      // Set translucent. (Default value is already true, so this can be removed if desired.)
      UINavigationBar.appearance().isTranslucent = true

      Zendesk.initialize(appId: "2c083506be2a443b2cccbdc0745eeced0f704955ed3a2165", clientId: "mobile_sdk_client_69a5173189be8d467104", zendeskUrl: "https://patghelp.zendesk.com")
      let ident = Identity.createAnonymous()
      Zendesk.instance?.setIdentity(ident)
      Support.initialize(withZendesk: Zendesk.instance)
            
      Purchases.debugLogsEnabled = true
      Purchases.configure(withAPIKey: "KregNMkYQPNEdasePHBbcDnciSQLpeTl")

      
      UIApplication.shared.setMinimumBackgroundFetchInterval(5*60)

      
      return true
   }
   
   func application(_ application: UIApplication,
                    performFetchWithCompletionHandler completionHandler:
                    @escaping (UIBackgroundFetchResult) -> Void) {
      // Check for new data.
      let newData = Notifications()
      let dougie = newData.getVerse()
   }
   
   
   // MARK: UISceneSession Lifecycle
   
   func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
      
      // Called when a new scene session is being created.
      // Use this method to select a configuration to create the new scene with.
      return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
   }
   
   func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
      // Called when the user discards a scene session.
      // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
      // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
   }
}

