//
//  UIViewExt.swift
//  PageViewController-Demo
//
//  Created by Douglas Palme on 4/8/21.
//  Copyright © 2021 Slicode. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
   
   func fadeKey() {
      // Move our fade out code from earlier
      UIView.animate(withDuration: 3.0, delay: 2.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
          self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
          }, completion: nil)
   }
   
    func fadeIn() {
        // Move our fade out code from earlier
      UIView.animate(withDuration: 1.0, delay: 0.5, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
            }, completion: nil)
    }

    func fadeOut() {
      UIView.animate(withDuration: 0.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
            }, completion: nil)
    }
}
