//
//  PageViewController.swift
//  PageViewController-Demo
//
//  Created by Shankar B S on 14/03/20.
//  Copyright © 2020 Slicode. All rights reserved.
//

import UIKit

class PageViewController: UIViewController {
    //1
    var pageIndex: Int = 0
    @IBOutlet weak var pageIndexLabel: UILabel!
    
   @IBOutlet weak var pageImageView: UIImageView!
   @IBOutlet weak var pageContentTextView: UITextView!
   @IBOutlet weak var lblVerse: UILabel!
   @IBOutlet weak var lblKey: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
   
   override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated);
       self.navigationController?.setToolbarHidden(true, animated: animated)
   }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      self.navigationController?.setToolbarHidden(false, animated: animated)
      self.lblVerse.fadeOut()
      self.lblKey.fadeOut()
      self.lblKey.textAlignment = .center
      self.lblKey.text = "John 3:16"
      
      self.lblVerse.textAlignment = .justified
      self.lblVerse.text = "For God so Love the World, that he gave his only begotten son, that whosoever believeth in him shall not perish, but have everlasting life."
      self.lblVerse.fadeIn()
      self.lblKey.fadeKey()
        //self.pageIndexLabel.text = "Page Index: \(pageIndex)"
      pageImageView.image = UIImage(named: "\(arc4random_uniform(50) + 1).png")

    }
}
