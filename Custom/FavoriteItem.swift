//
//  FavoriteItem.swift
//  Bible
//
//  Created by Douglas Palme on 6/16/21.
//
//
//

import Foundation
import Firebase
import FirebaseDatabase

struct FavItem {
   let Key: Int
   let Passage: String
   let Verse: String
   //let ref: DatabaseReference?
   
   
   init(Key: Int, Passage: String, Verse: String) {
      self.Key = Key
      self.Passage = Passage
      self.Verse = Verse
      //self.ref = nil
   }
   
   init(snapshot: DataSnapshot) {
      let snapshotValue = snapshot.value as! [String: AnyObject]
      Key = snapshotValue["K"] as! Int
      Passage = snapshotValue["P"] as! String
      Verse = snapshotValue["V"]  as! String
      //ref = snapshot.ref
   }
   
   func toAnyObject() -> Any {
      return [
         "Key": Key,
         "Passage": Passage,
         "Verse": Verse
      ]
   }
}
