//
//  Review.swift
//  Bible
//
//  Created by Douglas Palme on 7/13/21.
//

import Foundation

class Review {
   
   class func UpdateCt() {
      // Get count
      var myCt: Int = UserDefaults.standard.value(forKey: "ReviewCt") as! Int
      myCt += 1
      UserDefaults.standard.setValue(myCt, forKey: "ReviewCt")
   }
   
   class func checkCt() -> Bool {
      var retVal: Bool = false
      let ct = UserDefaults.standard.value(forKey: "ReviewCt")
      if ct == nil {
         retVal = false
      } else {
         let myCt = ct as! Int
         switch myCt {
         case 15:
            retVal = true
         case 50:
            retVal = true
         case 100:
            retVal = true
         case 200:
            retVal = true
         default:
            retVal = false
         }
      }
      return retVal
   }
}
