//
//  Notifications.swift
//  Bible
//
//  Created by Douglas Palme on 8/21/21.
//

import Foundation
import UserNotifications
import FirebaseDatabase

class Notifications: NSObject, UNUserNotificationCenterDelegate {
   
   var bodyText: String = ""
   let center = UNUserNotificationCenter.current()
   let dbRef = Database.database()

   
   func getVerse() {
      let pageIndex = Int.random(in: 1..<267)
      //let pageIndex = getData()
      let myRef2 = dbRef.reference().child("Verses1")
      myRef2.child(String(pageIndex)).observeSingleEvent(of: .value, with: { (snapshot) in
         let value = snapshot.value as? NSDictionary
         let oVerse = value?["V"] as? String ?? ""
         let oPassage = value?["P"] as? String ?? ""
         self.bodyText = oVerse
         }) { (error) in
            print(error.localizedDescription)
         }
   }
   
   func getData() -> Int {
      
      let calendar: Calendar = .autoupdatingCurrent
      let dayOfTheYear = Int(calendar.ordinality(of: .day, in: .year, for: Date())! )
      return dayOfTheYear
   }
   
   func setMessage() {
      let content = UNMutableNotificationContent()
      content.body = bodyText
   }
   
   func getNotifications() {
      center.getPendingNotificationRequests(completionHandler: { requests in
          for request in requests {
              print(request)
          }
      })
   }
   
   
}
