//
//  BookItem.swift
//  Bible
//
//  Created by Douglas Palme on 7/8/21.
//

import Foundation
import Firebase
import FirebaseDatabase

struct BkItem {
   let nm: String
   let ch: Int
   let key: Int
   
   
   init(nm: String, ch: Int, key: Int ) {
      self.nm = nm
      self.ch = ch
      self.key = key
   }
   
   init(snapshot: DataSnapshot) {
      let snapshotValue = snapshot.value as! [String: AnyObject]
      nm = snapshotValue["nm"] as! String
      ch = snapshotValue["ch"] as! Int
      key = snapshotValue["key"] as! Int
   }
   
   func toAnyObject() -> Any {
      return [
         "nm": nm,
         "ch": ch,
         "key": key
      ]
   }
}
