//
//  Menu.swift
//  Bible
//
//  Created by Douglas Palme on 7/8/21.
//

import Foundation
import Firebase
import FirebaseDatabase

struct MenuItem {
   let nm: String
   let act: String
   let key: Int
   
   init(nm: String, act: String, key: Int) {
      self.nm = nm
      self.act = act
      self.key = key
   }
   
   init(snapshot: DataSnapshot) {
      let snapshotValue = snapshot.value as! [String: AnyObject]
      nm = snapshotValue["nm"] as! String
      act = snapshotValue["act"] as! String
      key = snapshotValue["key"] as! Int
   }
   
   func toAnyObject() -> Any {
      return [
         "nm": nm,
         "act": act,
         "key": key
      ]
   }
}
