//
//  StudyItem.swift
//  Bible
//
//  Created by Douglas Palme on 7/15/21.
//

import Foundation
import Firebase
import FirebaseDatabase

struct StdyItem {
   let Book: Int
   let Chapter: Int
   let bVerse: Int
   let eVerse: Int
   let Title: String
   //let ref: DatabaseReference?
   
   
   init(Book: Int, Chapter: Int, bVerse: Int, eVerse: Int, Title: String) {
      self.Book = Book
      self.Chapter = Chapter
      self.bVerse = bVerse
      self.eVerse = eVerse
      self.Title = Title
   }
   
   init(snapshot: DataSnapshot) {
      let snapshotValue = snapshot.value as! [String: AnyObject]
      Book = snapshotValue["BK"] as! Int
      Chapter = snapshotValue["CH"] as! Int
      bVerse = snapshotValue["BV"] as! Int
      eVerse = snapshotValue["EV"] as! Int
      Title = snapshotValue["Tit"] as! String

   }
   
   func toAnyObject() -> Any {
      return [
         "Book": Book,
         "Chapter": Chapter,
         "bVerse": bVerse,
         "eVerse": eVerse,
         "Title": Title
      ]
   }
}
