//
//  Favorites.swift
//  Bible
//
//  Created by Douglas Palme on 6/14/21.
//

import Foundation
import SwiftUI



class Favorites {
   let defaults = UserDefaults.standard

   lazy var intArray = defaults.array(forKey: "Favorites") as? [Int] ?? []

   func contains(myValue: Int) -> Bool {
      if intArray.contains(myValue) {
         print(intArray)
         return true
      } else {
         print(intArray)
         return false
      }
   }
   
   func updFavorites() {
      defaults.setValue(intArray, forKey: "Favorites")
   }
   
   func addRemove(myValue: Int) -> String {
      var retVal: String = ""
      // first check to see if it exists
      if contains(myValue: myValue) == true {
         // Remove it
         if let i = intArray.firstIndex(of: myValue) {
            intArray.remove(at: i)
            updFavorites()
            retVal = "Removed"
         }
      } else {
         intArray.append(myValue)
         retVal = "Add"
         updFavorites()
      }
      return retVal
   }
   
   func add(myValue: Int) -> Bool {
      intArray.append(myValue)
      defaults.setValue(intArray, forKey: "Favorites")
      return true
   }
}
