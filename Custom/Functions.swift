//
//  Functions.swift
//  Bible
//
//  Created by Douglas Palme on 2/9/22.
//

import Foundation
class Functions {

   func getDayOfYear() -> Int {
      let calendar: Calendar = .autoupdatingCurrent
      let dayOfTheYear = Int(calendar.ordinality(of: .day, in: .year, for: Date())! )
      return dayOfTheYear
   }



}
