//
//  Music.swift
//  Bible
//
//  Created by Douglas Palme on 4/22/21.
//

import Foundation
import AVFoundation

class Music {

   static var isPlaying: Bool = false
   public static var backgrndSound = AVAudioPlayer()
   
      
   
   class func isMusicPlaying() -> Bool {
      isPlaying = UserDefaults.standard.bool(forKey: "isPlaying")
      return isPlaying
   }

   
   class func StartPlaying(musicName: String) {
      let musicTrack = musicName + ".mp3"
      let path = Bundle.main.path(forResource: musicTrack, ofType: nil)!
      let url = URL(fileURLWithPath: path)
      do {
         self.backgrndSound = try AVAudioPlayer(contentsOf: url)
         self.backgrndSound.numberOfLoops = -1
         self.backgrndSound.play()
         UserDefaults.standard.setValue(true, forKey: "isPlaying")
      } catch {
         // couldn't load file :(
      }
   }
   
   class func StopPlaying() {
      backgrndSound.pause()
      backgrndSound.stop()
      UserDefaults.standard.setValue(false, forKey: "isPlaying")
   }
   
   
   
   
   
}
